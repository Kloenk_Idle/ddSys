VERSIONFILE=./cmd/version.go

BINFOLDER=bin/

BINARY=./$(BINFOLDER)ddSys

VERSION=$(shell git describe --tags)
BUILD=$(shell git rev-parse HEAD)
BUILDDATE=$(shell git log -1 --format=%ci)

.DEFAULT_GOAL: all

all:	build-vers
	$(MAKE) build

build:
	go build -o $(BINARY)

update:
	git pull -r

build-vers:
	@echo "creating version file"
	@echo "package cmd" 					    >  $(VERSIONFILE)
	@echo "//constst for version" 			    >> $(VERSIONFILE)
	@echo "const (" 			  			    >> $(VERSIONFILE)
	@echo "   versionString = \"$(VERSION)\""   >> $(VERSIONFILE)
	@echo "   build         = \"$(BUILD)\""     >> $(VERSIONFILE)
	@echo "   buildDate     = \"$(BUILDDATE)\"" >> $(VERSIONFILE)
	@echo ")"									>> $(VERSIONFILE)


buildForAll: build-vers
	env GOOS="darwin" GOARCH="386" go build -o $(BINARY)_darwin_386
	env GOOS="darwin" GOARCH="amd64" go build -o $(BINARY)_darwin_64

	env GOOS="linux" GOARCH=386 go build -o $(BINARY)_linux_386
	env GOOS="linux" GOARCH=amd64 go build -o $(BINARY)_linux_64
	env GOOS="linux" GOARCH=arm go build -o $(BINARY)_linux_arm

	env GOOS="windows" GOARCH=386 go build -o $(BINARY)_win_386.exe
	env GOOS="windows" GOARCH=amd64 go build -o $(BINARY)_win_64.exe


signBin:
	gpg2 --sign $(BINARY)_darwin_386
	gpg2 --sign $(BINARY)_darwin_64

	gpg2 --sign $(BINARY)_linux_386
	gpg2 --sign $(BINARY)_linux_64
	gpg2 --sign $(BINARY)_linux_arm

	gpg2 --sign $(BINARY)_win_386.exe
	gpg2 --sign $(BINARY)_win_64.exe