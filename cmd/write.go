package cmd

import (
	"github.com/spf13/cobra"
	"io"
	"strconv"
	"os"
	"log"
	"path/filepath"
	"bufio"
	"strings"
	"fmt"
	"github.com/cheggaaa/pb"
	"runtime"
)

var disk string
var file string
var speed string
var mountInfoPath string

var defaultBufSize = int64(4 * 1024 * 1024)

var saveCmd = &cobra.Command{
	Use:"save",
	Short: "transfering the data from the disk to the file (!!BUGY!!)",
	Run: func(cmd *cobra.Command, args []string) {
		if version {
			fmt.Printf("%s\nversion: %s\nbuild: %s\nbuild date: %s\n on: %s, arch: %s\n", os.Args[0], versionString, build, buildDate, runtime.GOOS, runtime.GOARCH)
		} else {

			if disk == "" || file == "" {
				cmd.Help()
			}
			opts := ddOpts{
			}
			bs, err := ddAtio(speed)
			if err != nil {
				log.Fatal(err.Error())
				os.Exit(-1)
			}
			opts.bs = bs
			opts.dst = file
			opts.src = disk
			logThings("writing data from %s to %s with %v bps", opts.src, opts.dst, opts.bs)
			if err := dd(opts.src, opts.dst, opts.bs); err != nil {
				fmt.Println(err)
				os.Exit(-1)
			}
		}
	},
}

type FixedBuffer struct {
	w io.Writer
	buf []byte
}

func (f *FixedBuffer) ReadFrom(r io.Reader) (int, error) {
	return r.Read(f.buf)
}

func (f *FixedBuffer) Write(data []byte) (int, error) {
	return f.w.Write(data)
}

//the dd releated stuff
type ddOpts struct {
	src string
	dst string
	bs int64
}

func ddAtio(s string) (int64, error) {
	if len(s) < 2 {
		return strconv.ParseInt(s, 10, 64)
	}

	fac := int64(1)
	switch s[len(s) -2 :] {
	case "kB":
		fac = 1000
	case "MB":
		fac = 1000 * 1000
	case "GB":
		fac = 1000 *1000 * 1000
	case "TB":
		fac = 1000 *1000 * 1000 * 1000
	}

	if fac%10 == 0 {
		s = s[:len(s)-2]
	}

	switch s[len(s)-1:] {
	case "b":
		fac = 512
	case "K":
		fac = 1024
	case "M":
		fac = 1024 * 1024
	case "G":
		fac = 1024 * 1024 * 1024
	case "T":
		fac = 1024 * 1024 * 1024 * 1024
	}
	if fac%512 == 0 {
		s = s[:len(s)-1]
	}
	n, err := strconv.ParseInt(s, 10, 64)
	n *=fac
	return n, err
}


func NewFixedBuffer(w io.Writer, size int64) *FixedBuffer {
	return &FixedBuffer{
		w: w,
		buf: make([]byte, size),
	}
}

func sanityCheckDst(dstPath string) error {
	f, err := os.Open(mountInfoPath)
	if os.IsNotExist(err) {
		return nil
	}
	if err != nil {
		return err
	}
	defer f.Close()

	//resolv any symling to the target
	resolfDstPath, err := filepath.EvalSymlinks(dstPath)
	if err == nil {
		dstPath = resolfDstPath
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		l := strings.Fields(scanner.Text())
		if len(l) == 0 {
			continue
		}
		mountPoint := l[4]
		mountSrc := l[9]

		//resolv any symlinks in mountSrc
		resolMountSrc, err := filepath.EvalSymlinks(mountSrc)
		if err == nil {
			mountSrc = resolMountSrc
		}

		if strings.HasPrefix(mountSrc, dstPath) {
			return fmt.Errorf("%s is mounted on %s", mountSrc, mountPoint)
		}
	}
	return scanner.Err()
}

func dd(srcPath, dstPath string, bs int64) error {
	if bs == 0 {
		bs = defaultBufSize
	}

	src, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer src.Close()

	if err := sanityCheckDst(dstPath); err != nil {
		return err
	}

	dst, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer func() {
		dst.Sync()
		dst.Close()
	}()

	w := NewFixedBuffer(dst, bs)

	stat, err := src.Stat()
	if err != nil {
		return err
	}
	logThings("prepar for copying %v data", stat.Size())
	phbar := pb.New64(stat.Size()).SetUnits(pb.U_BYTES)
	phbar.Start()
	mw := io.MultiWriter(w, phbar)
	_, err = io.Copy(mw, src)
	return err
}


var restorCmd = &cobra.Command{
	Use:	"restore",
	Short: "uses a img and writes the data to the provided disk",
	Run: func(cmd *cobra.Command, args []string) {
		if version {
			fmt.Printf("%s\nversion: %s\nbuild: %s\nbuild date: %s\n on: %s, arch: %s\n", os.Args[0], versionString, build, buildDate, runtime.GOOS, runtime.GOARCH)
		} else {

			if disk == "" || file == "" {
				cmd.Help()
			}
			opts := ddOpts{
			}
			bs, err := ddAtio(speed)
			if err != nil {
				log.Fatal(err.Error())
				os.Exit(-1)
			}
			opts.bs = bs
			opts.dst = disk
			opts.src = file
			logThings("writing data from %s to %s with %v bps", opts.src, opts.dst, opts.bs)
			if err := dd(opts.src, opts.dst, opts.bs); err != nil {
				fmt.Println(err)
				os.Exit(-1)
			}
		}
	},
}