package cmd

import (
	"github.com/spf13/cobra"
	"fmt"
	"os"
	"os/signal"
	"log"
	"runtime"
)

var version bool

var RootCmd = &cobra.Command{
	Use: "ddSys",
	Short: "simple programm that makes a progress bar while transfering a img file",
	Run: func(cmd *cobra.Command, args []string) {
		if version {
			fmt.Printf("%s\nversion: %s\nbuild: %s\nbuild date: %s\n on: %s, arch: %s\n", os.Args[0], versionString, build, buildDate, runtime.GOOS, runtime.GOARCH)
		} else {
			cmd.Help()
		}
	},
}

func initFlags() {
	RootCmd.PersistentFlags().BoolVarP(&version, "version", "v", false, "prints version")
	RootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "V", false, "make verbose output")

	//saveflags
	saveCmd.Flags().StringVarP(&disk, "disk", "d", "", "select witch disk to save")
	saveCmd.Flags().StringVarP(&file, "file", "o", "", "select which file to save the disk to")
	saveCmd.Flags().StringVarP(&speed, "bs", "s", "1M", "select which speed to transfer data")
	saveCmd.Flags().StringVar(&mountInfoPath, "mountInfoPath", "/proc/self/mountinfo", "set the path of mountinfo")

	//restoreFlags
	restorCmd.Flags().StringVarP(&disk, "disk", "d", "", "select witch disk to write")
	restorCmd.Flags().StringVarP(&file, "file", "f", "", "select which file to write to the disk")
	restorCmd.Flags().StringVarP(&speed, "bs", "s", "1M", "select which speed to transfer data")
	restorCmd.Flags().StringVar(&mountInfoPath, "mountInfoPath", "/proc/self/mountinfo", "set the path of mountinfo")

	//init command
	RootCmd.AddCommand(saveCmd)
	RootCmd.AddCommand(restorCmd)
}


func Execute() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func () {
		for _ = range c {
			os.Exit(-2) //create asker
		}
	} ()
	initFlags()
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}


var verbose bool


func logThings(pattern string, x ...interface{}) {
	if verbose {
		pattern += "\n"
		log.Printf(pattern, x...)
	}
}